# import numpy as np
# import copy
# import re
import cv2
# from PIL import Image as pil
# from matplotlib import pyplot as plt
# import glob
import utils
import pytesseract

parameters = ["fbs", "2hpp", "hba1c", "uricavid", "ua", "triglycerides", "tgs", "cholesterol", "hdl",
              "ldl", "vldl", "calcium", "ca",
              "serumiron", "sgot", "sgpt","alt","ast", "tsh", "psa",
              "vitd", "b12", "bun", "creatinine",
              "hct", "hb", "rbc", "wbc", "dwbc", "plt",
              "mcv", "mch", "mchc", "rdw", "urea", "mxd", "platelets", "neutrophils", "lymphocytes", "mpv", "pct",
              "pdw",
              "hemoglobin", "hematocrite"]


def process_image(path):

    original_image = cv2.imread(path, 1)
    gray = cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY)
    big_gray = utils.image_resize(im=gray, width=int(gray.shape[0] * 2))
    removed_shadow = utils.remove_shadow_and_highlights(big_gray)
    sharped_image = utils.unsharp_mask(image=removed_shadow, kernel_size=(3, 3), sigma=2, amount=1,
                                       threshold=0)
    _, binary_image = cv2.threshold(sharped_image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    binary_image = utils.dilate(binary_image, (3, 3), 1)
    binary_image = utils.erode(binary_image, (3, 3), 1)
    data = pytesseract.image_to_data(binary_image, output_type=pytesseract.Output.DICT, config="--psm 1",
                                     lang="eng")
    data = utils.filter_data(data,0.0)

    test_boxes_data = utils.find_words_difflib(data, ["test"])
    result_boxes_data = utils.find_words_difflib(data, ["result"])
    all_tests_in_sheet = utils.find_boxes_covered_by_target_box(test_boxes_data, data)
    improved_test_data = utils.improve_detected_words(binary_image, all_tests_in_sheet)
    test_parameters = utils.find_words_difflib(improved_test_data, parameters)
    all_result_in_sheet = utils.find_boxes_covered_by_target_box(result_boxes_data, data)
    improved_result_data = utils.improve_detected_words(binary_image, all_result_in_sheet, True)

    test_parameters_with_result = utils.find_result_for_params(test_parameters, improved_result_data)
    return test_parameters_with_result

