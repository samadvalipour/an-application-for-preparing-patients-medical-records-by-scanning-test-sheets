import copy
import re
import numpy as np
import cv2
import pytesseract
import difflib

# def show(title, im):
#     cv2.imshow(title, im)
#     cv2.waitKey(0)
#     cv2.destroyAllWindows()


# skew correction
# def deskew(im):
#     coords = np.column_stack(np.where(im > 0))
#     angle = cv2.minAreaRect(coords)[-1]
#     if angle < -45:
#         angle = -(0 + angle)
#     else:
#         angle = -angle
#     (h, w) = im.shape[:2]
#     center = (w // 2, h // 2)
#     M = cv2.getRotationMatrix2D(center, angle, 1.0)
#     rotated = cv2.warpAffine(im, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
#     return rotated


# noise removal
# def remove_noise(im, s):
#     return cv2.medianBlur(im, s)


# dilation
def dilate(im, kernel_size=(5, 5), iteration=1):
    kernel = np.ones(kernel_size, np.uint8)
    return cv2.dilate(im, kernel, iterations=iteration)


# erosion
def erode(im, kernel_size=(5, 5), iteration=1):
    kernel = np.ones(kernel_size, np.uint8)
    return cv2.erode(im, kernel, iterations=iteration)

#
# # opening - erosion followed by dilation
# def opening(im, kernel_size=(5, 5)):
#     kernel = np.ones(kernel_size, np.uint8)
#     return cv2.morphologyEx(im, cv2.MORPH_OPEN, kernel)
#
#
# # closing - dilation followed by erosion
# def closing(im, kernel_size=(5, 5)):
#     kernel = np.ones(kernel_size, np.uint8)
#     return cv2.morphologyEx(im, cv2.MORPH_CLOSE, kernel)


def unsharp_mask(image, kernel_size=(5, 5), sigma=3, amount=3, threshold=0):
    """Return a sharpened version of the image, using an unsharp mask."""
    blurred = cv2.GaussianBlur(image, kernel_size, sigma)
    sharpened = float(amount + 1) * image - float(amount) * blurred
    sharpened = np.maximum(sharpened, np.zeros(sharpened.shape))
    sharpened = np.minimum(sharpened, 255 * np.ones(sharpened.shape))
    sharpened = sharpened.round().astype(np.uint8)
    if threshold > 0:
        low_contrast_mask  = np.absolute(image - blurred) < threshold
        np.copyto(sharpened, image, where=low_contrast_mask)
    return sharpened


def image_resize(im, width=None, height=None, inter=cv2.INTER_AREA):
    """Return a resized version of the image without changing it's aspect ratio"""
    # initialize the dimensions of the image to be resized
    dim = None
    (h, w) = im.shape[:2]
    if width is None and height is None:
        return im
    if width is None:
        # calculate the ratio of the height and construct the
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        # calculate the ratio of the width and construct the
        r = width / float(w)
        dim = (width, int(h * r))
    resized = cv2.resize(im, dim, interpolation=inter)
    return resized

#
# def improve_contrast(im):
#     lab_img = cv2.cvtColor(im, cv2.COLOR_BGR2LAB)
#     l, a, b = cv2.split(lab_img)
#     clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(12, 12))
#     clahe_img = clahe.apply(l)
#     updated_lab_img = cv2.merge((clahe_img, a, b))
#     clahe_img = cv2.cvtColor(updated_lab_img, cv2.COLOR_LAB2BGR)
#     return clahe_img


def remove_shadow_and_highlights(im):
    dilated_img = cv2.dilate(im, np.ones((7, 7), np.uint8))
    bg_img = cv2.medianBlur(dilated_img, 21)
    gray_sheet = 255 - cv2.absdiff(im, bg_img)
    norm_img = gray_sheet.copy()
    norm_img = cv2.normalize(gray_sheet, norm_img, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
    return norm_img


def enlargeـbox(size, x1, y1, x2, y2, value=7):
    # print("size : ", size)
    X1 = x1 - value if x1 - value >= 0 else 0
    Y1 = y1 - value if y1 - value >= 0 else 0
    X2 = x2 + value if x2 + value <= size[1] else size[1]
    Y2 = y2 + value if y2 + value <= size[0] else size[0]
    return X1, Y1, X2, Y2


def improve_detected_words(im, data, digit=False):
    data1 = {"left": [], "top": [], "width": [], "height": [], "conf": [], "text": []}
    n_boxes = len(data['text'])
    size = im.shape
    padding = 100
    for i in range(0, n_boxes):
        if int(float(data['conf'][i])) > -1:
            x1, y1, x2, y2 = enlargeـbox(size, data["left"][i], data["top"][i], data["left"][i] + data["width"][i],
                                         data["top"][i] + data["height"][i], value=2)
            bin = image_resize(im=im[y1:y2, x1:x2], height=100)
            bin = np.pad(bin, pad_width=padding, mode='constant', constant_values=255)
            if digit:
                text = pytesseract.image_to_string(bin,
                                                   config="tessedit_char_whitelist=123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ --psm 8")
                text = re.sub('[^0-9.A-Za-z]+', '', text).replace(' ', '.', 1)
            else:
                text = pytesseract.image_to_string(bin,
                                                   config="-c tessedit_char_whitelist=1234abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ- tessedit_char_blacklist=»;\~“,°!@#$?|*_+<>:©{}[]()=&^%$ --psm 8")
            if not text.replace('.', '', 1).replace(' ', '', 1).isdigit() and digit:
                continue
            if text == " " or text == "":
                continue
            data1["conf"].append(data['conf'][i])
            data1["text"].append(text.strip())
            data1["left"].append(x1)
            data1["top"].append(y1)
            data1["width"].append(x2 - x1)
            data1["height"].append(y2 - y1)

    return data1


def filter_data(data,lim=10.0):
    founded_data = {"left": [], "top": [], "width": [], "height": [], "conf": [], "text": []}
    for i, conf in enumerate(data["conf"]):
        if float(conf) >= lim:
            founded_data["text"].append(data["text"][i])
            founded_data["conf"].append(data["conf"][i])
            founded_data["height"].append(data["height"][i])
            founded_data["width"].append(data["width"][i])
            founded_data["top"].append(data["top"][i])
            founded_data["left"].append(data["left"][i])
    return founded_data


# def find_words(data, target_words):
#     founded_data = {"left": [], "top": [], "width": [], "height": [], "conf": [], "text": []}
#     for target in target_words:
#         for i, word in enumerate(data["text"]):
#             if target.lower() in word.strip().lower() and len(word.strip()) < len(target) + 5 and word not in \
#                     founded_data["text"]:
#                 founded_data["text"].append(data["text"][i].strip())
#                 founded_data["conf"].append(data["conf"][i])
#                 founded_data["height"].append(data["height"][i])
#                 founded_data["width"].append(data["width"][i])
#                 founded_data["top"].append(data["top"][i])
#                 founded_data["left"].append(data["left"][i])
#     return founded_data

def find_words_difflib(data, target_words):
    founded_data = {"left": [], "top": [], "width": [], "height": [], "conf": [], "text": []}
    for i, word in enumerate(data["text"]):
        m_s = detect_word(word=word,arr=target_words)
        if m_s != "":
            founded_data["text"].append(m_s)
            founded_data["conf"].append(data["conf"][i])
            founded_data["height"].append(data["height"][i])
            founded_data["width"].append(data["width"][i])
            founded_data["top"].append(data["top"][i])
            founded_data["left"].append(data["left"][i])
    return founded_data
def detect_word(word, arr):
    most = 0.0
    limit = 0.7
    most_similar = ""
    for w in arr :
        r = difflib.SequenceMatcher(a=w.lower(), b=word.strip().lower()).ratio()
        if r > most :
            most = r
            most_similar = w
    if most < limit :
        return ""
    return most_similar

def find_boxes_covered_by_target_box(targets, boxes):
    overlaped_data = {"left": [], "top": [], "width": [], "height": [], "conf": [], "text": []}
    for i in range(0, len(boxes["left"])):
        if isoverlap(boxes["left"][i], boxes["width"][i], targets):
            overlaped_data["text"].append(boxes["text"][i])
            overlaped_data["conf"].append(boxes["conf"][i])
            overlaped_data["height"].append(boxes["height"][i])
            overlaped_data["width"].append(boxes["width"][i])
            overlaped_data["top"].append(boxes["top"][i])
            overlaped_data["left"].append(boxes["left"][i])
    return overlaped_data


def isoverlap(b_left, b_width, t_s):
    for i in range(0, len(t_s["left"])):
        if are_separate(b_left, b_width, t_s["left"][i], t_s["width"][i]):
            return True
    return False


def find_result_for_params(paraeter_boxes, result_boxes):
    results_of_params = []
    for i in range(0, len(paraeter_boxes["text"])):
        for j in range(0, len(result_boxes["text"])):
            if abs(paraeter_boxes["top"][i] - result_boxes["top"][j]) < paraeter_boxes["height"][i] / 2:
                results_of_params.append((paraeter_boxes["text"][i], result_boxes["text"][j]))
    return results_of_params


def are_separate(a, a_w, x, x_w):  # r and s are ordered pairs
    b = a + a_w
    y = x + x_w
    if b < x or y < a:
        return False
    else:
        return True
