from pymongo import MongoClient


class db_connecton:
    def __init__(self):
        self.client = MongoClient('localhost', 27017)
        self.db = self.client.blood_experiment
        self.experiments = self.db.experiments

    def parameters_and_result_to_collection(self, parameters_and_result):
        collection = {}
        for key in parameters_and_result.keys():
            collection[str(key)] = parameters_and_result[key]
        return collection

    def add_document(self, parameter_and_results):
        document = self.parameters_and_result_to_collection(parameter_and_results)
        result = self.experiments.insert_one(document)
        return str(result.inserted_id)

    def find_data(self, username):
        data = self.experiments.find_one({'username': username})
        if data is None :
            return 0
        del data["_id"]
        return data
