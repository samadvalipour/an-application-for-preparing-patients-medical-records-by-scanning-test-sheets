//selecting all required elements
const dropArea = document.querySelector(".drag-area"),
  dragText = dropArea.querySelector("header"),
  button = dropArea.querySelector("button"),
  input = dropArea.querySelector("input");
let file; //this is a global variable and we'll use it inside multiple functions

button.onclick = () => {
  input.click(); //if user click on the button then the input also clicked
};
input.addEventListener("change", function () {
  setTimeout(function () {
    $("#overlay").fadeIn(300);
  }, 500);
  //getting user select file and [0] this means if user select multiple files then we'll select only the first one
  file = this.files[0];
  dropArea.classList.add("active");
  showFile(); //calling function
  setTimeout(() => {
    fileSend();
  }, 2000);
});

//If user Drag File Over DropArea
dropArea.addEventListener("dragover", (event) => {
  event.preventDefault(); //preventing from default behaviour
  dropArea.classList.add("active");
  dragText.textContent = "Release to Upload File";
});

//If user leave dragged File from DropArea
dropArea.addEventListener("dragleave", () => {
  dropArea.classList.remove("active");
  dragText.textContent = "Drag & Drop to Upload File";
});

//If user drop File on DropArea
dropArea.addEventListener("drop", (event) => {
  setTimeout(function () {
    $("#overlay").fadeIn(300);
  }, 500);
  event.preventDefault(); //preventing from default behaviour
  //getting user select file and [0] this means if user select multiple files then we'll select only the first one
  file = event.dataTransfer.files[0];
  showFile(); //calling function

  setTimeout(() => {
    fileSend();
  }, 2000);
});

function showFile() {
  let fileType = file.type; //getting selected file type
  let validExtensions = ["image/jpeg", "image/jpg", "image/png"]; //adding some valid image extensions in array
  if (validExtensions.includes(fileType)) {
    //if user selected file is an image file
    let fileReader = new FileReader(); //creating new FileReader object
    fileReader.onload = () => {
      let fileURL = fileReader.result; //passing user file source in fileURL variable
      let imgTag = `<img src="${fileURL}" alt="">`; //creating an img tag and passing user selected file source inside src attribute
      dropArea.innerHTML = imgTag; //adding that created img tag inside dropArea container
    };
    fileReader.readAsDataURL(file);
  } else {
    alert("This is not an Image File!");
    dropArea.classList.remove("active");
    dragText.textContent = "Drag & Drop to Upload File";
  }
}

function fileSend() {
  var form_data = new FormData();
  form_data.append("file", file);
  console.log(form_data);
  $.ajax({
    type: "POST",
    url: "/processimage",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    async: false,
    success: function (data) {
      console.log("Success!");
      console.log(data);
      show_result(data)
    },
  }).done(function () {
    setTimeout(function () {
      $("#overlay").fadeOut(300);
    }, 500);
//          show_result();
  });
}


function show_result(parameters) {
console.log(parameters)
var form = document.getElementById("form");
form.classList.remove("hidden");
  var params = document.getElementById("params");
  var result_label = document.createElement("div");
result_label.textContent = "نتیجه آزمایش‌ها را در صورت مقایرت با برگه آزمایش اصلاح کنید و سپس اطلاعات را ارسال کنید"
result_label.setAttribute("class", "col-12 alert alert-warning");
result_label.setAttribute("role","alert");
result_label.setAttribute("id","alert");
params.appendChild(result_label);
  console.log(params);
  for (let index = 0; index < parameters.length; index++) {
    console.log(parameters[index][0])
    txt = parameters[index][0].split("\\")[0]
    console.log(txt)
//    var inner_alert = '<p>نتیجه آزمایش‌ها را در صورت مقایرت با برگه آزمایش اصلاح کنید و سپس اطلاعات را ارسال کنید</p><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>'

//    result_label.innerHTML = inner_alert
    var data_div = document.createElement("div");
    data_div.setAttribute("class", "col-md-2 mb-3");

    var data_label = document.createElement("label");
    data_label.textContent = txt
    data_label.setAttribute("class", "form-label");
    data_label.setAttribute("for", txt);

    var data_input = document.createElement("input");
    data_input.setAttribute("type", "text");
    data_input.setAttribute("id", txt);
    data_input.setAttribute("name", txt);
    data_input.setAttribute("class", "form-control");
    data_input.setAttribute("placeholder", "مقدار");
    data_input.setAttribute("value", parameters[index][1]);
    data_div.appendChild(data_label);
    data_div.appendChild(data_input);
    params.appendChild(data_div);
  }
}
