function submition(){
username = $('#input').val()
username = username.replace(/\s/g, '');
if (username == null || username == "") {
      alert("لطفا نام کاربری را وارد کنید");
      return false;
    }
$.ajax({
    type: 'POST',
    url: '/search',
    data: {'username': username},
    async: false,
    success: function(response){
    if (response == 0){
    alert("اطلاعاتی برای نام کاربری وارد شده وجود ندارد")
    }
    else{
    show_result(response)
    }
    }
})
}
function jsonArrayTo2D(obj){
  let AoA = Object.keys(obj).map((key) => [key, obj[key]]);
  return AoA;
}
function show_result(data) {

parameters = jsonArrayTo2D(data)

var params = document.getElementById("params");
params.innerHTML = '';
  for (let index = 0; index < parameters.length; index++) {
    txt = parameters[index][0].split("\\")[0]
    console.log(txt)
    var data_div = document.createElement("div");
    data_div.setAttribute("class", "col-md-2 mb-3");

    var data_label = document.createElement("label");
    data_label.textContent = txt
    data_label.setAttribute("class", "form-label");
    data_label.setAttribute("for", txt);

    var data_input = document.createElement("input");
    data_input.setAttribute("type", "text");
    data_input.setAttribute("id", txt);
    data_input.setAttribute("name", txt);
    data_input.setAttribute("class", "form-control");
    data_input.setAttribute("placeholder", "مقدار");
    data_input.setAttribute("value", parameters[index][1]);

    data_input.setAttribute("disabled","disabled");
    data_div.appendChild(data_label);
    data_div.appendChild(data_input);
    params.appendChild(data_div);
  }
}
