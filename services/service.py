from flask import Flask, url_for, redirect, request, jsonify, render_template, send_from_directory
import sys

# sys.path.insert(0,
#                 '..\\src')
import main
from db_connection import db_connecton

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = r"C:\Temp"
app.debug = True

@app.route("/processimage", methods=["GET", "POST"])
def processimage():
    respons = ""
    if request.method == "POST":
        try:
            if 'file' in request.files:
                imageFile = request.files['file']
                savePath = ".\\images\\image.jpg"
                imageFile.save(savePath)
                data = main.process_image(savePath)
                respons = jsonify(data)
        except Exception as e:
            print(e)
        return respons


@app.route('/search', methods=['POST'])
def search():
    user = request.values.get("username")
    db_connection = db_connecton()
    data = db_connection.find_data(username=user)
    return jsonify(data)


@app.route('/save', methods=['POST'])
def save():
    db_connection = db_connecton()
    id = db_connection.add_document(request.form)
    return render_template("send_result.html")


@app.route('/analysis', methods=["GET"])
def analysis():
    if request.method == "GET":
        return render_template("analysis.html")


@app.route("/", methods=["GET"])
def home():
    return render_template("home.html")


@app.route("/sendresult", methods=["GET"])
def sendresult():
    return render_template("send_result.html")


@app.route("/about", methods=["GET"])
def test():
    return render_template("about.html")


if __name__ == "__main__":
    app.run()
