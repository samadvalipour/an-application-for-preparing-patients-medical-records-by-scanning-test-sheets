# An application for preparing patients medical records by scanning test sheets
<div id="top"></div>


<!-- ABOUT THE PROJECT -->
## About The Project
in this project we developed a web application for extracting important information from experimental test sheets.

## Screenshots
### input image
![sheet image](./screenshots/0.png)
### result
![sheet image](./screenshots/1.png)

## Software requirements
To run the project, you must install this softwares before any action.
* Python
  * [persian installation tuts](https://blog.faradars.org/%D9%86%D8%B5%D8%A8-%D9%BE%D8%A7%DB%8C%D8%AA%D9%88%D9%86-%D8%AF%D8%B1-%D9%88%DB%8C%D9%86%D8%AF%D9%88%D8%B2/)
  * [english installation tuts](https://phoenixnap.com/kb/how-to-install-python-3-windows) 
* MongoDB
  * [persian installation tuts](https://7learn.com/blog/how-to-install-mongodb-on-windows)
  * [english installation tuts](https://medium.com/@LondonAppBrewery/how-to-download-install-mongodb-on-windows-4ee4b3493514) 
* Tesseract
  * [english installation tuts](https://nanonets.com/blog/ocr-with-tesseract/#installingtesseract)
    


* [more information about tesseract in python](https://nanonets.com/blog/ocr-with-tesseract/#installingtesseract) 

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- GETTING STARTED -->
## Getting Started

To run the project for the first time follow this steps

### step 1

Create a virtual environment by running the view command in cmd that you opened at the root of the project
  ```sh
  python -m venv venv
  ```

### step 2

Now that we have a virtual environment, we need to activate it.
  ```sh
  .\venv\Scripts\activate.bat
  ```

### step 3 (Installing packages )
To install the required packages for the project. run this command
  ```sh
  pip install -r requirements.txt
  ```
<p align="right">(<a href="#top">back to top</a>)</p>


<!-- USAGE EXAMPLES -->
## run project

execute the following command to run the project
  ```sh
  python services\service.py
  ```
<p align="right">(<a href="#top">back to top</a>)</p>



